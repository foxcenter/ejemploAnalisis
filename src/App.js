import logo from './logo.svg';
import { Mybutton } from './components/button';
import './App.css';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h3> Pruebas de CI/CD</h3>
        <Mybutton/>
      </header>
    </div>
  );
}

export default App;
